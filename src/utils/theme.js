import { createTheme } from '@material-ui/core/styles';

const theme = createTheme({
  spacing: 8,
  // There could be more theme options
})

export default theme;