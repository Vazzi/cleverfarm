import axios from 'axios';

const API_URL = 'http://localhost:3001'; // Better to put into some env file

/**
 * Axios GET function from API server that is defined in config as API_URL
 *
 * @param {string } path of the wanted resource
 * @returns {Promise} Axios promise
 */
export const get = (path) => {
  return axios.get(API_URL + path);
};
