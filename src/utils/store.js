import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducers from '../stores/reducers';

const middlewares = composeWithDevTools(applyMiddleware(thunk));

export const store = createStore(reducers, middlewares);
