import { createSelector } from 'reselect';
import { EMPTY_SENSOR_ID } from '../constants/sensors';

/**
 * Basic structure of the empty sensor
 *
 * @type {{name: string, coordinates: number[], description: string, id: string}}
 */
const emptySensor = {
  id: EMPTY_SENSOR_ID,
  name: 'Sensor does not exists',
  description: '',
  coordinates: {
    longitude: 0,
    latitude: 0
  }
};

/**
 * Get sensor data or empty array
 *
 * @param {object} state Global state
 * @returns {Array} sensors data or empty array if no data in store
 */
const getSensorsData = (state) => state.sensors.data || [];

/**
 * Selector that returns normalized selectors data from store of sensors
 *
 * @param {object} state Global state
 * @returns {Array} list of sensors
 */
export const getSensors = createSelector(getSensorsData, (data) =>
  data.map(({ id, name, description, coordinates }) => ({
    id,
    name,
    description,
    coordinates: { latitude: coordinates[1], longitude: coordinates[0] }
  }))
);

/**
 * Selector that returns normalized sensor data for given id. If no sensor is
 * found under given id the dummy emptySensor is returned.
 *
 * @param {object} state Global state
 * @param {string} id ID of the sensor
 * @returns {{name: string, coordinates: number[], description: string, id: string}} sensor structure
 */
export const getSensor = (state, id) => {
  const sensors = getSensors(state).filter((item) => item.id === id);
  if (sensors.length === 0) {
    return emptySensor;
  }
  return sensors[0];
};

/**
 * Selector that returns normalized error value from store of sensors
 *
 * @param {object} state Global state
 * @returns {number} Normalized error value from store of sensors
 */
export const getError = (state) => state.sensors.error;

/**
 * Selector that returns isLoading value from store of sensors
 *
 * @param {object} state Global state
 * @returns {boolean} IsLoading value from store of sensors
 */
export const getIsLoading = (state) => state.sensors.isLoading;

/**
 * Selector that returns isInvalid value from store of sensors
 *
 * @param {object} state Global state
 * @returns {boolean} IsInvalid value from store of sensors
 */
export const getIsInvalid = (state) => state.sensors.isInvalid;
