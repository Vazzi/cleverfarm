import React from 'react';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Switch, Route, Redirect } from 'react-router-dom';

import theme from '../utils/theme';

import MainPage from './MainPage/MainPage';
import SensorDetailPage from './SensorDetailPage/SensorDetailPage';

const useStyles = makeStyles((theme) => ({
  content: {
    paddingTop: theme.spacing(4)
  }
}));

/**
 * App component
 * Represents main component of the application
 *
 * @returns {React} Application react node
 */
const app = () => {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6">CleverFarm - sensors</Typography>
        </Toolbar>
      </AppBar>
      <Container className={classes.content}>
        <Switch>
          <Route path="/sensor/:id">
            <SensorDetailPage />
          </Route>
          <Route path="/" exact>
            <MainPage />
          </Route>
          <Redirect to="/" />
        </Switch>
      </Container>
    </ThemeProvider>
  );
};

export default app;
