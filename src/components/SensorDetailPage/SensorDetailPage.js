import React from 'react';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import { useHistory, withRouter } from 'react-router';

import { getSensor } from '../../selectors/sensors';
import MapWrapper from './MapWrapper';
import { EMPTY_SENSOR_ID } from '../../constants/sensors';

/**
 * SensorDetailPage component
 * Represents the detail of the sensor with detail information about sensor
 * and location in map
 *
 * @param {object} props React component props
 * @param {object} props.sensor object containing sensor information
 * @returns {React} the component node
 */
const sensorDetailPage = ({ sensor }) => {
  const { id, name, description, coordinates } = sensor;
  const history = useHistory();
  const showMap = !(id === EMPTY_SENSOR_ID);

  const handleBack = () => {
    history.push('/');
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Button onClick={handleBack}>Go Back</Button>
      </Grid>
      <Grid item xs={12}>
        <Typography variant="h4">{name}</Typography>
        <Typography variant="body1">{description}</Typography>
      </Grid>
      {showMap && (
        <Grid item xs={12}>
          <MapWrapper
            latitude={coordinates.latitude}
            longitude={coordinates.longitude}
          />
        </Grid>
      )}
    </Grid>
  );
};

const mapStateToProps = (state, props) => ({
  sensor: getSensor(state, props.match.params.id)
});

export default withRouter(connect(mapStateToProps)(sensorDetailPage));
