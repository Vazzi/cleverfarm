import React, { useEffect, useState, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Map from 'ol/Map';
import View from 'ol/View';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { fromLonLat } from 'ol/proj';
import { Circle, Fill, Style } from 'ol/style';

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.down('md')]: {
      height: 300,
      width: 600
    },
    [theme.breakpoints.down('xs')]: {
      height: 300,
      width: 300
    },
    [theme.breakpoints.up('md')]: {
      height: 400,
      width: 800
    }
  }
}));

// Style of the point on the map
const pointStyle = new Style({
  image: new Circle({
    radius: 5,
    fill: new Fill({ color: 'blue' })
  })
});

// TileLayer of the map
const tileLayer = new TileLayer({
  source: new OSM()
});

/**
 * MapWrapper component
 * Represent map that contain a marker of the given location
 *
 * @param {object} props React component props
 * @param {number} props.latitude coordinate
 * @param {number} props.longitude coordinate
 * @returns {React} the component node
 */
const mapWrapper = ({ latitude, longitude }) => {
  const classes = useStyles();

  const [map, setMap] = useState();

  const mapElement = useRef();
  const mapRef = useRef();
  mapRef.current = map;

  useEffect(() => {
    const point = fromLonLat([longitude, latitude]);
    const iconFeature = new Feature({ geometry: new Point(point) });
    const vectorSource = new VectorSource({ features: [iconFeature] });

    const vectorLayer = new VectorLayer({
      source: vectorSource,
      style: pointStyle
    });

    const map = new Map({
      layers: [tileLayer, vectorLayer],
      target: mapElement.current,
      view: new View({
        center: point,
        zoom: 14
      }),
      controls: []
    });
    setMap(map);
  }, [longitude, latitude]);

  return <div ref={mapElement} className={classes.root} />;
};

export default mapWrapper;
