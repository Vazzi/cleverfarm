import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import { useHistory } from 'react-router';

const useStyles = makeStyles(() => ({
  button: {
    float: 'right'
  },
  description: {
    textAlign: 'justify'
  },
  root: {
    height: '100%'
  }
}));

/**
 * SensorsList component
 * Represents the list of sensors and link to it's detail
 *
 * @param {object} props React component props
 * @param {number} props.id ID if the sensor
 * @param {string} props.name Name of sensor
 * @param {string} props.description Description of the sensor
 * @returns {React} the component node
 */
const sensorCard = ({ id, name, description }) => {
  const classes = useStyles();
  const history = useHistory();

  const handleDetailClick = (id) => {
    history.push('/sensor/' + id);
  };

  return (
    <Card className={classes.root}>
      <CardContent>
        <Grid
          container
          direction="column"
          justifyContent="space-between"
          alignItems="stretch"
        >
          <Grid item xs={12}>
            <Typography variant="h4">{name}</Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body1">{description}</Typography>
          </Grid>
          <Grid item xs={12}>
            <Button
              className={classes.button}
              onClick={() => handleDetailClick(id)}
            >
              Detail
            </Button>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

sensorCard.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string
};

sensorCard.defaultProps = {
  description: ''
};

export default sensorCard;
