import React from 'react';
import { shallow } from 'enzyme';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

import SensorCard from './SensorCard';

describe('<SensorCard /> component', () => {
  let props;
  let wrapper;

  beforeEach(() => {
    props = {
      id: '12',
      name: 'Test name',
      description: 'Test desc'
    };

    wrapper = shallow(<SensorCard {...props} />);
  });

  describe('renders', () => {
    it('contains name on first place', () => {
      expect(wrapper.find(Typography).first().text()).toBe('Test name');
    });

    it('contains description', () => {
      expect(wrapper.find(Typography).at(1).text()).toBe('Test desc');
    });

    it('contains button with detail text', () => {
      expect(wrapper.find(Button)).toHaveLength(1);
      expect(wrapper.find(Button).text()).toBe('Detail');
    });
  });

  describe('detail button', () => {
    it('has correct href attribute to redirect to correct sensor detail', () => {
      expect(wrapper.find(Button).props().href).toBe('/sensor/12');
    });
  });
});
