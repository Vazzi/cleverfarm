import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

import SensorCard from './SensorCard';
import { getError, getIsLoading, getSensors } from '../../../selectors/sensors';
import { fetchSensorsIfNeeded } from '../../../stores/actions/sensors';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(1)
  }
}));
/**
 * SensorsList component
 * Represents the list of sensors and link to it's detail
 *
 * @param {object} props React component props
 * @param {Array} props.sensors array of sensors
 * @param {boolean} props.isLoading if the data is loading
 * @param {string} props.error error message if call of API fail
 * @param {Function} props.loadSensors function to make call to API to get sensors
 * @returns {React} the component node
 */
const sensorsList = ({ sensors, isLoading, error, loadSensors }) => {
  const classes = useStyles();

  React.useEffect(() => {
    loadSensors();
  }, [loadSensors]);

  return (
    <>
      <Grid
        container
        spacing={2}
        justifyContent="flex-start"
        className={classes.root}
      >
        {isLoading && <CircularProgress />}
        {error && <Typography variant="body1">{error}</Typography>}
        {sensors &&
          sensors.map((sensor) => (
            <Grid item xs={12} md={6} lg={4} key={sensor.id}>
              <SensorCard
                id={sensor.id}
                name={sensor.name}
                description={sensor.description}
              />
            </Grid>
          ))}
      </Grid>
    </>
  );
};

sensorsList.propTypes = {
  sensors: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      description: PropTypes.string
    })
  ),
  isLoading: PropTypes.bool.isRequired,
  error: PropTypes.string
};

const mapStateToProps = (state) => ({
  isLoading: getIsLoading(state),
  sensors: getSensors(state),
  error: getError(state)
});

const mapDispatchToProps = {
  loadSensors: fetchSensorsIfNeeded
};

export default connect(mapStateToProps, mapDispatchToProps)(sensorsList);
