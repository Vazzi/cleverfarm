import React from 'react';
import Typography from '@material-ui/core/Typography';

import SensorsList from './SensorsList/SensorsList';

/**
 * MainPage component
 * Represents the Main page with list of sensors
 *
 * @returns {React} the component node
 */
const mainPage = () => {
  return (
    <>
      <Typography variant="h4">Available sensors:</Typography>
      <SensorsList />
    </>
  );
};

export default mainPage;
