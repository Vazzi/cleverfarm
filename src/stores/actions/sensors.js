import { get } from '../../utils/api';
import {
  SENSORS_ERROR,
  SENSORS_REQUEST,
  SENSORS_SUCCESS
} from '../../constants/actions';
import { getIsLoading, getIsInvalid } from '../../selectors/sensors';

/**
 *  Action to start request
 *
 * @returns {object} Action
 */
const sensorsRequest = () => ({
  type: SENSORS_REQUEST
});

/**
 *  Action to success response
 *
 * @param {object} data from response
 * @returns {object} Action
 */
const sensorsSuccess = (data) => ({
  type: SENSORS_SUCCESS,
  data
});

/**
 * Action to error response
 *
 * @param {number} error from response
 * @returns {object} Action
 */
const sensorsError = (error) => ({
  type: SENSORS_ERROR,
  error
});

/**
 * Check whether the request needs to be run
 *
 * @param {object} state of sensors
 * @returns {boolean} Return boolean
 */
const shouldFetchSensors = (state) => {
  if (getIsLoading(state)) {
    return false;
  }

  return getIsInvalid(state);
};

/**
 * Request to the server to obtain the sensors
 *
 * @returns {Promise} Return promise
 */
const fetchSensors = () => {
  return (dispatch) => {
    dispatch(sensorsRequest());
    return get('/sensors')
      .then((res) => dispatch(sensorsSuccess(res.data)))
      .catch((error) =>
        dispatch(
          sensorsError(
            error.response ? error.response.statusText : 'Server is unreachable'
          )
        )
      );
  };
};

/**
 * Execute request to the server to obtain the sensors
 * only when shouldFetchSensors function return true
 *
 * @returns {Promise} Return promise
 */
export const fetchSensorsIfNeeded = () => {
  return (dispatch, getState) => {
    if (shouldFetchSensors(getState())) {
      return dispatch(fetchSensors());
    }

    return Promise.resolve();
  };
};
