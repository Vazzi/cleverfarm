import { createReducer } from '../../utils/reducer';
import {
  SENSORS_ERROR,
  SENSORS_REQUEST,
  SENSORS_SUCCESS
} from '../../constants/actions';

/**
 * Initial state for reducer
 */
const initialState = {
  isLoading: false,
  isInvalid: true,
  data: null,
  error: null
};

/**
 * Function create "sensors" reducer
 *
 * @returns {Function}
 */
export default createReducer(initialState, {
  [SENSORS_REQUEST]: (state) => ({
    ...state,
    isLoading: true,
    error: null
  }),

  [SENSORS_SUCCESS]: (state, { data }) => ({
    ...state,
    isLoading: false,
    isInvalid: false,
    data: data,
    error: null
  }),

  [SENSORS_ERROR]: (state, { error }) => ({
    ...state,
    isLoading: false,
    isInvalid: false,
    data: null,
    error: error
  })
});
