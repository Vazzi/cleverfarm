import { combineReducers } from 'redux';

import sensors from './sensors';

// Not necessary to use this solution in this case because only one reducer is
// created. However I used it to show how can be more reducers combined
// in index.js file
const reducers = combineReducers({
  sensors
});

export default reducers;
